__author__ = 'alan.francis'

import sys
import os
import time
import datetime

current_milli_time = lambda: int(round(time.time() * 1000))  # a quick lambda to get time in milliseconds

root_folder = '/Volumes/Official/Stuff/ZHacks'
ignore_pattern = '.DS_Store'
pathsep = '/'
fileName = 'fileList'
extn = '.txt'
new_file_list = []
old_file_list = []


if len(sys.argv) > 1:
    root_folder = sys.argv[1]

if root_folder.endswith('/'):
    outFile = root_folder+fileName
else:
    outFile = root_folder+pathsep+fileName

time_stamp = ''
noNewFileAdded = True
noFilesRemoved = True
newFileCount = 0
deletedFileCount = 0

file_check_start_time = datetime.datetime.now()
print('File check began at', file_check_start_time)

if os.path.exists(outFile+extn):
    with open(outFile+extn, encoding='UTF-8') as a_file:
        for line in a_file:
            old_file_list.append(line.strip())
    time_stamp += '~'+str(current_milli_time())

# print('====>',outFile+time_stamp+extn)
with open(outFile+time_stamp+extn, mode='w', encoding='UTF-8') as b_file:
    for root, dirs, files in os.walk(root_folder, topdown=True):
        for name in files:
            if ignore_pattern not in name and '._' not in name:
                # print(os.path.join(root, name))
                if not str(os.path.join(root, name)).endswith(outFile+time_stamp+extn):
                    b_file.write(os.path.join(root, name)+'\n')
                    new_file_list.append(os.path.join(root, name))
                else:
                    print('removing', outFile+time_stamp+extn, 'from new file list')

if os.path.join(outFile+extn) in new_file_list:
    new_file_list.remove(os.path.join(outFile+extn))
    print('removing', outFile+extn, 'from new file list')

print('Old file count:', len(old_file_list))
print('New file count:', len(new_file_list))

filesremove = root_folder+pathsep+'filesRemoved-'+str(current_milli_time())+extn
filesadd = root_folder+pathsep+'filesAdded-'+str(current_milli_time())+extn
for oldFile in old_file_list:
    if oldFile not in new_file_list:
        noFilesRemoved = False
        deletedFileCount += 1
        with open(filesremove, mode='a', encoding='UTF-8') as rf:
            rf.write(oldFile+'\n')

for newFile in new_file_list:
    if newFile not in old_file_list and len(old_file_list) > 0:
        noNewFileAdded = False
        newFileCount += 1
        with open(filesadd, mode='a', encoding='UTF-8') as af:
            af.write(newFile+'\n')

file_check_end_time = datetime.datetime.now()
print('File check ended at', file_check_end_time)

if noFilesRemoved and noNewFileAdded:
    print('No change in '+root_folder)
    if time_stamp != '':
        os.remove(outFile+time_stamp+extn)
elif noFilesRemoved:
    print('No files have been removed')
    print('but there are ', newFileCount, 'new files')
elif noNewFileAdded:
    print('No new Files have been added')
    print('but', deletedFileCount, 'files have been removed')

print('File check completed in', (file_check_end_time-file_check_start_time))
