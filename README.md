# README #

You share a large set of documents in a pen drive to someone, then they cut and paste a few files without realising, you don't realise that you've a few files missing until much later. It has happened to everyone I know.

So to do a file audit of any folder, I wrote this script. This script can make a list of all the files under all the sub folders of a particular folder. Later on you can run the same script to see if any files are missing or if any new files have been added.

You will get the added/removed file names in appropriately named files inside your folder


run the script using the format below:


```
#!shell
python3.4 FolderAudit.py /Volumes/Personal/FolderToAudit

```



####Note:
* Script as of now ignore ".DS_Store" and "._*" patterns.
* Developed with python3.4